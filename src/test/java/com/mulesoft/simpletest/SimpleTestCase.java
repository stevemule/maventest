package com.mulesoft.simpletest;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.Test;
import org.mule.tck.junit4.FunctionalTestCase;

public class SimpleTestCase extends FunctionalTestCase {
	
	@Test
	public void testThis() throws Exception {
		httpSender("test", "GET", "");
	}
	
	protected String getConfigResources()
	{
	    return "mule-config.xml";
	}
	
	protected void httpSender(String url, String method, String payload) throws Exception {
		
		URL obj = new URL("http://localhost:8082/api" + "/"+url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		// optional default is GET
		con.setRequestMethod(method);
		if (!payload.equals("")) {
			byte[] outputInBytes = payload.getBytes("UTF-8");
			OutputStream os = con.getOutputStream();
			os.write( outputInBytes );    
			os.close();
		}
 
		//add request header
		con.setRequestProperty("Content-Length", "0");
		con.setRequestProperty("Content-Type", "application/json");
 

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	    assertNotNull(response.toString());
	    assertEquals("200", ""+con.getResponseCode()); 
		
		
//		MuleClient client = new MuleClient(muleContext);
//	    Map<String, Object> properties = new HashMap<String, Object>();
//	    properties.put("http.method", method);
//	    properties.put("Content-Length", "0");
//	    properties.put("Content-Type", "application/json");
//	    if (method.equalsIgnoreCase(HTTP_METHOD_POST)) {
//	        properties.put("TE", "Chunked");
//	    }
//	    logger.info("Using Mule Native App API: "+nativeAppHostUrl + ", with Timeout: "+nativeAppHostTimeout);
//	    MuleMessage result = client.send(nativeAppHostUrl + "/"+url, payload, properties, Integer.parseInt(nativeAppHostTimeout));
//	    assertNotNull(result.getPayloadAsString());
//	    assertEquals("200", result.getInboundProperty("http.status")); 
	}

}
